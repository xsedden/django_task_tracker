#WEB interface task tracker.

###What can this application do?
* Create user
* Add user
* Remove user
* Login user
* Exit user
* Create tasks
* Demonstrating all the tasks that user has
* Edit and remove tasks
* Create recurrent tasks
* Remove recurrent tasks
* Demonstrating all the recurrent tasks that user has

###How to install?
*    $ python preparation.py
*    $ pip install -r requirements.txt
*    $ python manage.py migrate

###How to run?
* $ python manage.py runserver

Made by Mikhail Shchasnovich. Group 653502.