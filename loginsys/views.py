# -*- coding: utf-8 -*-


from django.contrib.auth.models import User
from django.template.context_processors import csrf
from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm

import logging
logger = logging.getLogger(__name__)

# Create your views here.


def login(request):
    """ Login to user by username and password. """
    args = {}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            logger.info("User {} login".fromat(user))
            return redirect('/')
        else:
            args['login_error'] = 'Пользователь не найден'
            logger.info(args.get('login_error'))
            return render(request, 'login.html', args)
    else:
        return render(request, 'login.html', args)


def logout(request):
    """ Logout from user. """
    logger.info("User {} logout".fromat(auth.get_user(request).username))
    auth.logout(request)
    return redirect('/')


def register(request):
    """ Register new user by username and password and repeat password. """
    args = {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    if request.POST:
        newuser_form = UserCreationForm(request.POST)
        if newuser_form.is_valid():
            user = newuser_form.save()
            auth.login(request, user)
            logger.info('User {0} was created'.format(User.get_username(request.user)))
            return redirect('/')
        else:
            logger.warning('The form for creating a user wasnt valid')
            args['form'] = newuser_form
    return render(request, 'register.html', args)
