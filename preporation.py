import os


def make_folder_for_configs():
    if not os.path.exists(os.path.join(os.path.expanduser('~'), '.track')):
        os.mkdir(os.path.join(os.path.expanduser('~'), '.track'))
    if not os.path.exists(os.path.join(os.path.expanduser('~'), '.track/web_logs')):
        os.mkdir(os.path.join(os.path.expanduser('~'), '.track/web_logs'))

if __name__ == '__main__':
    make_folder_for_configs()
