from django.contrib import auth

def get_rec_by_user(request, list_rec):
    out_list = []
    for rec in list_rec:
        if auth.get_user(request).username == rec.creater:
            out_list.append(rec)
    return out_list
