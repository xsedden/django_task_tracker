# Generated by Django 2.0.6 on 2018-06-23 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recurrent', '0002_auto_20180622_1726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recurrent',
            name='create_date',
            field=models.CharField(blank=True, default='20180623', max_length=8),
        ),
        migrations.AlterField(
            model_name='recurrent',
            name='startdate',
            field=models.CharField(blank=True, default='20180623', max_length=8),
        ),
    ]
