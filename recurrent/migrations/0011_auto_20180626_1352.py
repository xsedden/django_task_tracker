# Generated by Django 2.0.6 on 2018-06-26 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recurrent', '0010_auto_20180625_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recurrent',
            name='create_date',
            field=models.CharField(default='20180626', max_length=8),
        ),
        migrations.AlterField(
            model_name='recurrent',
            name='startdate',
            field=models.CharField(default='20180626', max_length=8),
        ),
    ]
