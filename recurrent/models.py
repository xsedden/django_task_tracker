from django.db import models
from datetime import datetime
from django.utils import timezone

# Create your models here.


class Recurrent(models.Model):
    TYPES = (
        ('w', 'Week'),
        ('d', "Day"),
        ('m', 'Month')
    )
    name = models.CharField(max_length=20, default='')
    text = models.TextField(max_length=200, default='')
    startdate = models.DateTimeField(default=timezone.now)
    parent_id = models.PositiveIntegerField(blank=True, default=0)
    priority = models.PositiveIntegerField(default=10)
    create_date = models.DateTimeField(default=timezone.now)
    typetask = models.CharField(max_length=1, choices=TYPES)
    creater = models.CharField(max_length=20, default='', blank=True)
