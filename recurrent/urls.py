from django.contrib import admin
from django.urls import path, include

from . import views

app_name = 'recurrent'
urlpatterns = [
    path('createrec/', views.create_rec, name='create_rec'),
    #path('editrec/', views.edit_rec, name='edit_rec'),
    path('removerec/<int:rec_id>/', views.remove, name='remove_rec'),
    path('show/<int:page_number>/', views.show_rec, name='show_rec'),
    path('show/', views.show_rec, name='show_rec'),
]
