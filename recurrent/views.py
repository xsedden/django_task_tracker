from django.shortcuts import render, get_object_or_404, redirect
from .forms import RecurrentForm
from .models import Recurrent
from main.lib.recurrent_task import Recurrent, remove_rec, check_rec
from main.lib.convert_data import get_rec, get_rec_creater_by_id
from main.lib.edit import check_deadline
from datetime import datetime
from django.template.context_processors import csrf
from django.contrib import auth
from django.core.paginator import Paginator
from recurrent.funcs.funcs import get_rec_by_user
import logging


logger = logging.getLogger(__name__)
# Create your views here.


def create_rec(request):
    """ Create recurrent task for current user. """
    rec_form = RecurrentForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        args['form'] = rec_form
        args['username'] = auth.get_user(request).username
        if request.POST:
            new_rec_form = RecurrentForm(request.POST)
            if new_rec_form.is_valid():
                rec = new_rec_form.cleaned_data
                rec.update({"create_date": datetime.strftime(datetime.now(), "%Y%m%d")})
                new_rec = Recurrent(rec.get('typetask'), rec.get('text'), rec.get('parent_id'), rec.get('name'), rec.get('startdate'), args['username'])
                new_rec.add_rec()
                logger.info('New recurrent task "{0}" was created by user {1} with startdate {2}'.format(rec.get('typetask'), auth.get_user(request).username, rec.get('startdate')))
                return redirect('/')
            else:
                args['form'] = new_rec_form
                args['messages'] = ["Некорректные данные"]
                logger.warning('Not valid data for form "create recurrent" by user {}'.format(auth.get_user(request).username))
        return render(request, 'createrec.html', args)
    else:
        logger.warning('Unauthorized user try to create recurrent task')
        return redirect('/auth/login')


def remove(request, rec_id):
    """ Remove recurrent task by id. """
    if auth.get_user(request).username == get_rec_creater_by_id(rec_id):
        remove_rec(rec_id)
        logger.info('Recurrent task with id {0} was removed by {1}'.format(rec_id, auth.get_user(request).username))
    else:
        logger.warning('User try to remove recurrent task with access')
    return redirect('/recurrent/show/')


def show_rec(request, page_number=1):
    """ Show all recurrent tasks for current user. """
    check_rec()
    check_deadline()
    if auth.get_user(request).username:
        args = {'username': auth.get_user(request).username}
        list_rec = get_rec()
        tasks = get_rec_by_user(request, list_rec)
        current_page = Paginator(tasks, 10)
        tasks = current_page.page(page_number)
        args.update({'tasks': tasks})
        logger.info('Show recurrent tasks for user {0} on page {1}'.format(args.get('username'), page_number))
        return render(request, 'show.html', args)
    else:
        logger.warning('Unauthorized user try to show recurrent tasks')
        return redirect('/auth/login')

