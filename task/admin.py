from django.contrib import admin

# Register your models here.
from .models import Task, Admin, Person

admin.site.register(Task)
admin.site.register(Admin)
admin.site.register(Person)
