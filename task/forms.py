from django.forms import ModelForm, Textarea, Form, DateTimeField, BooleanField, ChoiceField
from .models import Task, Person, Admin
from django.utils import timezone
from datetime import datetime


class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ['create_date', 'status', 'creater']



class AdminForm(ModelForm):
    class Meta:
        model = Admin
        exclude = ['task']


class PersonForm(ModelForm):
    class Meta:
        model = Person
        exclude = ['task']


class FilterForm(Form):
    choice = (
        ("Person", "Person"),
        ("Admin", "Admin"),
        ("Creater", "Creater"),
    )
    lowest_date = DateTimeField(label="lowest date", initial= datetime(2000,1,1))
    upest_date = DateTimeField(label="upest date", initial=timezone.now)
    without_finished = BooleanField(label="without finished", initial=False, required=False)
    access = ChoiceField(choices=choice, initial="Person")
