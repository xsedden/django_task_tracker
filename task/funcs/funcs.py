from django.contrib import auth
from main.lib.convert_data import get_ids_parents_by_id, get_famaly_by_id, get_person_by_id,get_admin_by_id, get_createdate_by_id
from main.lib.person import check_show, check_access_admin, check_access_creater, check_access_person
from datetime import datetime

def get_tasks_by_user(request, list_task):
    out_list = []
    user = auth.get_user(request).username
    for task in list_task:
        if user == task.creater or user in get_person_by_id(task.id) or user in get_admin_by_id(task.id):
            out_list.append(task)
    return out_list


def get_task_spases_by_user(request, list_task):
    tasks = get_tasks_by_user(request, list_task)
    spase = ""
    for task in tasks:
        task.name = _spases_tree(request, task) + task.name
    return tasks


def _spases_tree(request, task):
    spase = ''
    const = u'\u2022'
    for taske in get_ids_parents_by_id(task.id):
        if check_show(taske, auth.get_user(request).username):

            spase += '<span style="color: #FF4500">{}</span>'.format(const)
    return spase


def get_tree(request, list_task):
    out_list = []
    tasks = get_task_spases_by_user(request, list_task)
    for task in tasks:
        if get_famaly_by_id(task.id) == 0:
            out_list.append(task)
            out_list = _rec_show(task, tasks, out_list)
    return out_list


def _rec_show(task, tasks, out_list):
    for taske in tasks:
        if get_famaly_by_id(taske) == task.id:
            out_list.append(taske)
            out_list = _rec_show(taske, tasks, out_list)
    return out_list


def get_filtered_tasks(request, tasks, filter_fields):
    tasks = get_tasks_by_user(request, tasks)
    out_list = []
    user = auth.get_user(request).username
    upest_date = int(datetime.strftime(filter_fields.get("upest_date"), "%Y%m%d"))
    lowest_date = int(datetime.strftime(filter_fields.get("lowest_date"), "%Y%m%d"))
    without_finished = filter_fields.get("without_finished")
    access = filter_fields.get("access")
    for task in tasks:
        if check_access_creater(task.id, user) and access == "Creater":
            if without_finished:
                if task.status != 1:
                    if get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                        out_list.append(task)
            else:
                if get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                    out_list.append(task)
        elif check_access_admin(task.id, user) and access == "Admin":
            if without_finished:
                if task.status != 1 and get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                    out_list.append(task)
            else:
                if get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                    out_list.append(task)
        elif check_access_person(task.id, user) and access == "Person":
            if without_finished:
                if task.status != 1 and get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                    out_list.append(task)
            else:
                if get_createdate_by_id(task.id) >= lowest_date and get_createdate_by_id(task.id) <= upest_date:
                    out_list.append(task)
    return out_list
