from django.db import models
from datetime import datetime
from django.utils import timezone


class Task(models.Model):
    STATUSES = ((0, 'Expects'),
                (1, 'Finished'),
                (2, 'Failed'))
    name = models.CharField(max_length=20, default='')
    text = models.TextField(max_length=200, default='')
    deadline = models.DateTimeField(default=timezone.now)
    parent_id = models.PositiveIntegerField(default=0)
    priority = models.PositiveIntegerField(default=10)
    create_date = models.DateTimeField(default=timezone.now)
    status = models.PositiveIntegerField(default=0, choices=STATUSES)
    creater = models.CharField(max_length=20, default='', blank=True)


class Admin(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='admins')
    admins_name = models.CharField(max_length=20, default='', blank=True)


class Person(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE,  related_name='persons')
    persons_name = models.CharField(max_length=20, default='', blank=True)


