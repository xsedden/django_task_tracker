from django.urls import path
from . import views


app_name = 'task'
urlpatterns = [
    path('createtask/', views.create_task, name='create_task'),
    path('show/<int:page_number>/', views.show_tasks, name='show_tasks'),
    path('show/', views.show_tasks, name='show_tasks'),
    path('', views.index, name='index'),
    path('remove/<int:task_id>/', views.remove_task, name='remove_task'),
    path('edit/<int:task_id>/', views.edit_task, name='edit_task'),
    path('finish/<int:task_id>/', views.finish, name='finish'),
    path('addadmin/<int:task_id>/', views.add_admin, name='add_admin'),
    path('addperson/<int:task_id>/', views.add_person, name='add_person'),
    path('removeadmin/<int:task_id>/', views.remove_admin, name='remove_admin'),
    path('removeperson/<int:task_id>/', views.remove_person, name='remove_person')
]
