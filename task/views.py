from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.urls import reverse
from .models import Task
from django.contrib import auth
from .forms import TaskForm, AdminForm, PersonForm, FilterForm
from django.template.context_processors import csrf
from main.lib.convert_data import add_task_to_file, TaskTable, get_tasks,get_creater_by_id
from main.lib.edit import delete_task, edit_text, edit_deadline, edit_name, edit_priority, edit_famaly,finish_task, check_deadline
from main.lib.recurrent_task import check_rec
from datetime import datetime
from main.lib.person import add_access_person, add_access_admin, remove_access_person, get_admin_by_id,remove_access_admin
from task.funcs.funcs import get_tree, get_filtered_tasks
from django.core.paginator import Paginator
from main.lib.exceptions import UserError, TaskError, IDError
import logging


logger = logging.getLogger(__name__)


def index(request):
    """ Render main page """
    args = {}
    args.update(csrf(request))
    args['username'] = auth.get_user(request).username
    logger.info("Show main page")
    return render(request, 'task/index.html', args)


def create_task(request):
    """ Create new task for current user. """
    task_form = TaskForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        args['form'] = task_form
        args['username'] = auth.get_user(request).username
        if request.POST:
            new_task_form = TaskForm(request.POST)
            if new_task_form.is_valid():
                task = new_task_form.cleaned_data
                name = task.get("name").replace('<', '&lt').replace('>', '&gt')
                task.update({"status": 0, "create_date": datetime.now(),"creater" : args['username'], "name": name})
                add_task_to_file(task)
                logger.info("Task {0} was created by {1} with deadline {2}".format(name, args["username"], task["deadline"]))
                return redirect('/task/show')
            else:
                args['form'] = new_task_form
                args['messages'] = ["Некорректные данные"]
                logger.warning("Data for create task form wasnt valid")
        return render(request, 'task/createtask.html', args)
    else:
        logger.warning('Unauthorized user try create task')
        return redirect('/auth/login')


def remove_task(request, task_id):
    """ Remove task by id. """
    user = auth.get_user(request).username
    if user == get_creater_by_id(task_id):
        delete_task(task_id, auth.get_user(request).username)
        logger.info("Remove task with id {0} by {1}".format(task_id, user))
    else:
        logger.warning('User try remove task without access')
    return redirect('/task/show')


def edit_task(request, task_id):
    """ Edit task by id. """
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        user = auth.get_user(request).username
        if user == get_creater_by_id(task_id) or user in get_admin_by_id(task_id):
            inst = Task.objects.get(pk=task_id)
            task_form = TaskForm(instance=inst)
            args['id'] = task_id
            args['form'] = task_form
            args['username'] = user
            if request.POST:
                new_task_form = TaskForm(request.POST)
                if new_task_form.is_valid():
                    task = new_task_form.cleaned_data
                    try:
                        edit_famaly(task_id, task.get("parent_id"), args['username'])
                        edit_priority(task_id, task.get("priority"), args['username'])
                        edit_name(task_id, task.get("name"), args['username'])
                        edit_deadline(task_id, task.get("deadline"), args['username'])
                        edit_text(task.get("text"),task_id, args['username'] )
                        logger.info("Edit task with id {0} by {1}".format(task_id, user))
                        return redirect('/task/show')
                    except TaskError as te:
                        error = [te.msg]
                        args['messages'] = error
                        logger.warning(error)
                        return render(request, 'task/edittask.html', args)
                else:
                    args['form'] = new_task_form
                    args['messages'] = ["Некорректные данные"]
                    logger.warning("Data for edit task form wasnt valid")
            return render(request, 'task/edittask.html', args)
        else:
            return redirect('/task/show')
    else:
        logger.warning('Unauthorized user try edit task')
        return redirect('/auth/login')


def finish(request, task_id):
    """ Finish task by id or if task was finished set expects status """
    user = auth.get_user(request).username
    if user == get_creater_by_id(task_id) or user in get_admin_by_id(task_id):
        finish_task(task_id, auth.get_user(request).username)
    else:
        logger.warning('User try finish task without access')
    return redirect('/task/show')


def add_person(request, task_id):
    """ Add person for task by id. """
    person_form = PersonForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        user = auth.get_user(request).username
        if user == get_creater_by_id(task_id) or user in get_admin_by_id(task_id):
            args['id'] = task_id
            args['form'] = person_form
            args['username'] = user
            if request.POST:
                new_person_form = PersonForm(request.POST)
                if new_person_form.is_valid():
                    person = new_person_form.cleaned_data
                    add_access_person(person.get("persons_name"), task_id, args['username'])
                    logger.info("Person {0} was added for task {1}".format(person.get("persons_name"), task_id))
                    return redirect('/')
                else:
                    args['form'] = new_person_form
                    args['messages'] = ["Некорректные данные"]
                    logger.warning("Data for add person form wasnt valid")
            return render(request, 'task/addperson.html', args)
        else:
            return redirect('/task/show')
    else:
        logger.warning('Unauthorized user try add person for task')
        return redirect('/auth/login')


def remove_person(request, task_id):
    """ Remove person from task by id. """
    person_form = PersonForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        user = auth.get_user(request).username
        if user == get_creater_by_id(task_id) or user in get_admin_by_id(task_id):
            args['id'] = task_id
            args['form'] = person_form
            args['username'] = user
            if request.POST:
                new_person_form = PersonForm(request.POST)
                if new_person_form.is_valid():
                    person = new_person_form.cleaned_data
                    remove_access_person(person.get("persons_name"), task_id, args['username'])
                    logger.info("Person {0} was removed from task {1}".format(person.get("persons_name"), task_id))
                    return redirect('/task/show')
                else:
                    args['form'] = new_person_form
                    args['messages'] = ["Некорректные данные"]
                    logger.warning("Data for remove person form wasnt valid")
            return render(request, 'task/removeperson.html', args)
        else:
            return redirect('/task/show')
    else:
        logger.warning('Unauthorized user try remove person for task')
        return redirect('/auth/login')


def add_admin(request, task_id):
    """ Add admin for task by id. """
    admin_form = AdminForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        user = auth.get_user(request).username
        if user == get_creater_by_id(task_id):
            args['id'] = task_id
            args['form'] = admin_form
            args['username'] = user
            if request.POST:
                new_admin_form = AdminForm(request.POST)
                if new_admin_form.is_valid():
                    admin = new_admin_form.cleaned_data
                    add_access_admin(admin.get("admins_name"), task_id, args['username'])
                    logger.info("Admin {0} was added for task {1}".format(person.get("admins_name"), task_id))
                    return redirect('/task/show')
                else:
                    args['form'] = new_admin_form
                    args['messages'] = ["Некорректные данные"]
                    logger.warning("Data for add admin form wasnt valid")
            return render(request, 'task/addadmin.html', args)
        else:
            return redirect('/task/show')
    else:
        logger.warning('Unauthorized user try add admin for task')
        return redirect('/auth/login')


def remove_admin(request, task_id):
    """ Remove admin from task by id. """
    admin_form = AdminForm()
    args = {}
    args.update(csrf(request))
    if auth.get_user(request).username:
        user = auth.get_user(request).username
        if user == get_creater_by_id(task_id):
            args['id'] = task_id
            args['form'] = admin_form
            args['username'] = user
            if request.POST:
                new_admin_form = AdminForm(request.POST)
                if new_admin_form.is_valid():
                    admin = new_admin_form.cleaned_data
                    remove_access_admin(admin.get("admins_name"), task_id, args['username'])
                    logger.info("Admin {0} was removed from task {1}".format(person.get("admins_name"), task_id))
                    return redirect('/task/show')
                else:
                    args['form'] = new_admin_form
                    args['messages'] = ["Некорректные данные"]
                    logger.warning("Data for remove admin form wasnt valid")
            return render(request, 'task/removeadmin.html', args)
        else:
            return redirect('/task/show')
    else:
        logger.warning('Unauthorized user try remove admin for task')
        return redirect('/auth/login')


def show_tasks(request, page_number=1):
    """ Show tasks by filter(access, without finished, create date) for current user. """
    check_rec()
    check_deadline()
    if auth.get_user(request).username:
        args = {'username': auth.get_user(request).username}
        list_rec = get_tasks()
        tasks = get_tree(request, list_rec)
        form = FilterForm()
        if request.method == 'POST':
            form = FilterForm(request.POST)
            if form.is_valid():
                filter_fields = form.cleaned_data
                tasks = get_filtered_tasks(request, tasks, filter_fields)
                logger.info("Set filters for show by {}".format(args.get('username')))
            else:
                args['messages'] = ["Некорректные данные"]
                logger.warning("Data for filter form wasnt valid")

        args.update({'form': form})

        current_page = Paginator(tasks, 10)
        tasks = current_page.page(page_number)
        args.update({'tasks': tasks})

        logger.info('Show tasks by user {0} on page {1}'.format(args.get('username'), page_number))
        return render(request, 'task/show.html', args)
    else:
        logger.warning('Unauthorized user try to show tasks')
        return redirect('/auth/login')
